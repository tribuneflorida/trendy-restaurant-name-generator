<?php 
	include ('restaurantGenerator.php');
	//Prevents caching
	header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
	header("Cache-Control: no-store, no-cache, must-revalidate");
	header("Cache-Control: post-check=0, pre-check=0", false);
	header("Pragma: no-cache");
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<title>Trendy Restaurant Name Generator - South Florida Sun-Sentinel</title> 
	<meta http-equiv="X-UA-Compatible" content="IE=7"/>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>
    <meta name="description" content="Need an uber-trendy, sophisticated name for your restaurant? Look no further!"/>
    <meta name="keywords" content="restaurant names, trendy restaurant names, top restaurant names, restaurant name generator"/>
	<meta property="og:image" content="http://www.trbimg.com/img-5011bed1/turbine/sfl-impress-your-friends-with-our-ubertrendy-restaurant-name-generator-20120726/187/16x9,"/>
	<meta name="y_key" content="87156028997d6d53"> 
	<meta name="msvalidate.01" content="AAC9C18F70AC386BC4DCF4DDF9BF1786">
	<link rel="shortcut icon" href="http://www.sun-sentinel.com/favicon.ico"/>
	<link rel="icon" type="image/ico" href="http://www.sun-sentinel.com/favicon.ico"/>
	
	<script>
		((((window.trb || (window.trb = {})).data || (trb.data = {})).metrics || (trb.data.metrics = {})).thirdparty = {
			pageName: 'sfss:features:trendy-restaurant-name-generator:articleproject.',
			channel: 'features',
			server: 'interactive.sun-sentinel.com',
			hier1: 'sun-sentinel:features',
			hier2: 'features',
			prop1: 'D=gn',
			prop2: 'features',
			prop38: 'articleproject',
			prop57: 'D=c38',
			eVar20: 'sun-sentinel',
			eVar21: 'D=c38',
			eVar34: 'D=ch',
			eVar35: 'D=gn',
			events:''
		});
	</script>
	
	<style type="text/css">
		body {padding-top: 35px; color:#3D3D3D;}
		.rg-container {font-family: Georgia, Times New Roman, Times, serif; margin: auto; padding: 0 20px; text-align: center;}
		.rg-label {font-size: 28px; padding: 12px;}
		.rg-restaurantname {font-size: 65px; padding: 20px; background-color: #F0F8FF; margin-bottom: 40px;}
		.rg-refreshbutton {font-size: 20px; text-decoration: none; display: block; max-width: 350px; line-height: 100%; background-color: #36648B; padding: 15px; border: 1px solid #36648B; color:#FFF; margin: auto;}
		.rg-refreshbutton:visited  {color:#FFF; text-decoration: none;}
		.rg-refreshbutton:hover {border: 1px solid #36648B; background-color: #FFF; color:#36648B;}
		.rg-credit {text-align: right; font-size: 14px; color #666666; margin: 20px 0 12px 0; font-family: Georgia, Times New Roman, Times, serif;}
		.rg-sharetools {margin: 40px 0; }
	</style>
</head>
<body
        data-ad-path="/4011/trb.sunsentinel/features"
        data-ad-ptype="sf"
        data-ad-iframe-url="/ad-iframe.html"
>
	<script src="http://www.sun-sentinel.com/thirdpartyservice" async></script>
	<div data-trb-thirdpartynav></div>
	<div class="rg-container">
		<h1 class="rg-label">Trendy Restaurant Name Generator</h1>
			
		<div class="rg-restaurantname"><?php echo $restaurantName ?></div>

		<a class="rg-refreshbutton" href="/restaurant-name-generator/?<?php echo substr(str_shuffle("abcdefghijklmnopqrstuvwxyz0123456789"), 0, 5); //Cache bypass ?>">Choose another. <?php echo $buttonMessage; ?></a>
		
		<div class="rg-sharetools">
			<script type="text/javascript">var switchTo5x=true;</script>
			<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
			<script type="text/javascript">stLight.options({publisher: "27fc6069-a990-45f6-bc3c-01c3538312c8"}); </script>
			<span class='st_fblike_hcount' displayText='Facebook Like'></span>
			<span class='st_facebook_hcount' displayText='Facebook'></span>
			<span class='st_twitter_hcount' displayText='Tweet'></span>
			<span class='st_pinterest_hcount' displayText='Pinterest'></span>
			<span class='st_googleplus_hcount' displayText='Google +'></span>
			<span class='st_email_hcount' displayText='Email'></span>
			<span class='st_sharethis_hcount' displayText='ShareThis'></span>
		</div>
		
		<div class="rg-credit">(Danny Sanchez/Sun Sentinel)</div>
	</div>
</body>
</html>