<?php
//###TRENDY RESTAURANT NAME GENERATOR
//This script generates a trendy restuarant name using combinations of a random number, a celebrity chef, a trendy type of food and/or a trendy-sounding location. There are various templates that use bits and pieces of the above.
//Generates a random number
	$restaurantNumber = rand(12,989);

//Choose a chef.
	$chefList = array("Emeril Lagasse's", "Michelle Bernstein's", "Anthony Bourdain's", "Gordon Ramsay's", "Bobby Flay's", "Tom Collicchio's", "Mario Batali's", "Paula Deen's", "Wolfgang Puck's", "Rachael Ray's", "Martin Yan's");
	
	$chef = $chefList[array_rand($chefList)];
	
	
//Chooses two food types without repetition.

	$foodTypeList = array("Raw Bar", "Fusion", "Martini", "Sushi Boat", "Tapas", "Craft Beer", "Fondue", "Mixology", "Vegan", "Gluten-Free", "Organic", "Farm to Table", "Gourmet Burger", "Ceviche", "Molecular Gastronomy", "Flavored Salt", "Artisan Chocolate", "Agave", "Foie Gras", "Vegan Doughnut", "Fancy Doughnut", "Umami", "Korean Taco", "Gourmet Cupcake", "Tofu", "Bacon Sweets", "Artisan Bakery", "Non-GMO");

	$foodType1 = $foodTypeList[array_rand($foodTypeList)];
	$foodType2 = $foodTypeList[array_rand($foodTypeList)];
	
	do {
		$foodType2 = $foodTypeList[array_rand($foodTypeList)];
	}
	while ($foodType1 == $foodType2);
	
//Chooses two locations without repetition.

	$locationList = array ("Hookah Lounge", "Trattoria", "Bistro", "Cafe", "Brasserie", "Gastropub", "Buffet", "Waterfront", "Sports Bar", "Food Truck", "Emporium", "Factory", "Noshery", "Fishery", "Chophouse", "Small Plate", "Al Fresco", "Woodfire", "Supper Club", "Kitchen", "Rooftop", "Cellar", "Brew Pub", "Ice Bar", "Ultra Lounge", "Ultra Lounge", "Coffee House", "Wine Bar", "Microbrewery", "Cigar Bar", "Cantina", "Tequila Bar", "Sandwicherie", "Creperie", "Chateau", "Speakeasy");

	$location1 = $locationList[array_rand($locationList)];
	$location2 = $locationList[array_rand($locationList)];
	do {
		$location2 = $locationList[array_rand($locationList)];
	}
	while ($location1 == $location2);

////#################generateWord() - Creates random words with vowel insertion to make them seem more like real words.
function generateWord () {
	
	$consonantList = array("b", "c", "d", "f", "g", "h", "j", "k", "l", "m", "n", "p", "r", "s", "t", "v", "w", "y", "z");
	
	$vowelList = array("a", "e", "i", "o", "u");

	//Get random letters from the list
	$randomWord = "";
	$consonant1 = $consonantList[array_rand($consonantList)];
	$consonant2 = $consonantList[array_rand($consonantList)];
	$consonant3 = $consonantList[array_rand($consonantList)];
	$vowel1 = $vowelList[array_rand($vowelList)];
	$vowel2 = $vowelList[array_rand($vowelList)];
	$vowel3 = $vowelList[array_rand($vowelList)];
		
	//Now we string the letters together into a word using one of three templates. 
	$randomWordNumber = rand(0,1);
	
	//Word Template 1: {C}{V}{C}{V}{C}
	if ($randomWordNumber == 0) {
	$randomWord = strtoupper($consonant1) . $vowel1 . $consonant2 . $vowel2. $consonant3;
	}
	//Word Template 2: {V}{C}{V}{C}{V}
	else if ($randomWordNumber == 1) {
	$randomWord = strtoupper($vowel1) . $consonant1 . $vowel2 . $consonant3 . $vowel3;
	}

	return $randomWord;
}
//#################END FUNCTION generateWord()

//Generate a word and check if it fits a list of foul words that fit the template.
do {
	$randomWord = generateWord();
}
while ($randomWord == "Cum" || $randomWord == "Feg" || $randomWord == "Fuk" || $randomWord == "Gay" || $randomWord == "Jap" || $randomWord == "Kuk" || $randomWord == "Sex" || $randomWord == "Tit" || $randomWord == "Yed" || $randomWord == "Cipa" || $randomWord == "Dego" || $randomWord == "Dupa" || $randomWord == "Hore" || $randomWord == "kike" || $randomWord == "Mofo" || $randomWord == "Nazi" || $randomWord == "Paki" || $randomWord == "Pula" || $randomWord == "Pule" || $randomWord == "Puta" || $randomWord == "Puto" || $randomWord == "Suka" || $randomWord == "Faget" || $randomWord == "Fagit" || $randomWord == "Feces" || $randomWord == "Fukah" || $randomWord == "Fuken" || $randomWord == "Fuker" || $randomWord == "Fukin" || $randomWord == "Jisim" || $randomWord == "Kurac" || $randomWord == "Mibun" || $randomWord == "Azis" || $randomWord == "Penas" || $randomWord == "Penis" || $randomWord == "Penus" || $randomWord == "Polac" || $randomWord == "Polak" || $randomWord == "Semen");


//DISPLAY THE RESTAURANT NAME USING ONE OF FOUR RANDOMLY SELECTED TEMPLATES
$templateNumber = rand(0,5);

//Template: Emeril Lagasse's Raw Bar Waterfront
if($templateNumber == 0) {
	$restaurantName = $chef . " " . $foodType1 . " " . $location1;
}
//Template: TESNO Fancy Doughnut and Martini
if($templateNumber == 1) {
	$restaurantName = $randomWord . " " . $foodType1 . " & " . $foodType2;
}

//Template: Foie Gras 291
if($templateNumber == 2) {
	$restaurantName = $foodType1 . " " . $restaurantNumber;
}

//Template: Vegan Emporium
if($templateNumber == 3) {
	$restaurantName = $foodType1 . " " . $location1;
}

//Template: Tapas Buffet and Chophouse

if($templateNumber == 4) {
	$restaurantName = $foodType1 . " " . $location1 . " & " . $location2;
}

//Template: Tapas Buffet + Chophouse

if($templateNumber == 5) {
	$restaurantName = $foodType1 . " " . $location1 . " + " . $location2;
}


//Displays a button message

$buttonMessageList = array("Needs to be more <i>caliente</i>.", "Lacks swankiness.", "It's missing that <i>je ne sais quoi</i>.", "That was so 2011.", "That name is beneath me.", "My friends already eat there.", "A Kardashian would never eat there.", "It sounds like cheap fast food.");

$buttonMessage = $buttonMessageList[array_rand($buttonMessageList)];

?>